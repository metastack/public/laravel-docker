FROM php:8.1-fpm as base

# Create app directory
RUN mkdir -p /var/www/html
WORKDIR /var/www/html

# Add laravel user
RUN groupadd -g 1000 laravel && \
    useradd -G laravel -g laravel -d /home/laravel -s /bin/bash laravel && \
    mkdir -p /home/laravel && \
    chown laravel:laravel /home/laravel

# Install gnupg
RUN apt-get update && apt-get install -y gnupg

# Add custom repos
RUN curl https://www.postgresql.org/media/keys/ACCC4CF8.asc \
    | gpg --dearmor \
    | tee /etc/apt/trusted.gpg.d/apt.postgresql.org.gpg >/dev/null \
    # Postgres repo
    && echo "deb http://apt.postgresql.org/pub/repos/apt/ bookworm-pgdg main" > /etc/apt/sources.list.d/postgresql.list

# Install system dependencies and updates
RUN apt-get update && apt-get upgrade --with-new-pkgs -y && apt-get install -y \
    ca-certificates \
    curl \
    git \
    libc-client-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libkrb5-dev \
    libmagickwand-dev \
    libonig-dev \
    libpng-dev \
    libpq-dev \
    libwebp-dev \
    libxml2-dev \
    libyaml-dev \
    libzip-dev \
    p7zip-full \
    postgresql-client \
    rsync \
    tini \
    unzip \
    zip \
    && apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/*

# Configure imap
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl

# Configure gd
RUN docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp

# Install PHP extensions
RUN docker-php-ext-install \
    bcmath \
    calendar \
    exif \
    gd \
    imap \
    opcache \
    pcntl \
    pdo_mysql \
    pdo_pgsql \
    xml \
    zip

# Install imagick
RUN pecl install imagick && docker-php-ext-enable imagick

# Install redis
RUN pecl install redis && docker-php-ext-enable redis

# Install yaml
RUN pecl install yaml && docker-php-ext-enable yaml

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Copy some configs in place
# DO OVERRIDE DEFAULT DEVELOPMENT CONFIG IN PRODUCTION!!
RUN cp $PHP_INI_DIR/php.ini-development /usr/local/etc/php/php.ini
COPY configs/opcache.ini /usr/local/etc/php/conf.d/opcache.ini
COPY configs/laravel.ini /usr/local/etc/php/conf.d/laravel.ini

FROM base as development

# Install xdebug
RUN pecl install xdebug && docker-php-ext-enable xdebug

# Disable composer memory limit in dev
ENV COMPOSER_MEMORY_LIMIT=-1
