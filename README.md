# Laravel php-fpm docker image

## Supported tags

-   `8.1`, `8.1-xdebug`
-   `8.2`, `8.2-xdebug`
-   `8.3`, `8.3-xdebug`
-   `8.4`, `8.4-xdebug`

## Minor changes and security fixes

The images are weekly (every wednesday on 04:00 Europe/Berlin) rebuilded with the latest minor changes and security fixes provided by the php-fpm base image.

## Warning

This image is using the default php-development.ini.

So for production you have to override the `/usr/local/etc/php/php.ini` with production settings!!! See the following example.

## License

View [license information](https://gitlab.com/metastack/public/laravel-docker/-/blob/master/LICENSE) for the software contained in this image.
